﻿/*!
 * Bundesrat.js v.01
 * http://www.der-burtchen.de/bundesrat/bundesrat.html [for now	]
 *
 * Uses Jquery
 *
 * Author: Christian Burtchen
 */

var BR = {};

BR.coalitions = [
  "cdu-pro-fdp",
  "csu",
  "csu-fdp",
  "cdu-fdp",
  "cdu",
  "cdu-spd",
  "spd-cdu",
  "cdu-fdp-gruene",
  "cdu-spd-gruene",
  "cdu-gruene",
  "gruene-cdu",
  "spd-fdp",
  "spd-fdp-gruene",
  "spd",
  "spd-gruene",
  "gruene-spd",
  "spd-gruene-ssw",
  "spd-linke",
  "linke-spd-gruene",
  "spd-linke-gruene"
];

BR.federalCoalitions = {
  "27. 9. 1998" : "spd-gruene",
  "22. 9. 2002" : "spd-gruene",
  "18. 9. 2005" : "cdu-spd",
  "27. 9. 2009" : "cdu-fdp",
  "22. 9. 2013" : "cdu-spd"
};

// Defines all the German states, their abbreviation, amount of votes and the allocation of the election date and the respective governmental coalitions
BR.laender = {
  bb:  { votes: 4, "27. 9. 1998" : "spd", "5. 9. 1999": "spd-cdu", "19. 9. 2004": "spd-cdu", "27. 9. 2009": "spd-linke", "14. 9. 2014": "spd-linke"},
  be:  { votes: 4, "27. 9. 1998": "cdu-spd", "10. 10. 1999": "cdu-spd", "16. 6. 2001": "spd-gruene minderheit", "21. 10. 2001": "spd-linke", "17. 9. 2006": "spd-linke", "18. 9. 2011": "spd-cdu", "17. 9. 2016": "spd-linke-gruene"},
  bw:  { votes: 6, "27. 9. 1998": "cdu-fdp", "25. 3. 2001": "cdu-fdp", "26. 3. 2006": "cdu-fdp", "27. 3. 2011": "gruene-spd", "13. 3. 2016": "gruene-cdu"},
  by:  { votes: 6, "27. 9. 1998": "csu", "21. 9. 2003": "csu", "28. 9. 2008": "csu-fdp", "15. 9. 2013" : "csu"},
  hb:  { votes: 3, "27. 9. 1998": "spd-cdu", "6. 6. 1999": "spd-cdu", "25. 5. 2003": "spd-cdu", "13. 5. 2007": "spd-gruene", "22. 5. 2011": "spd-gruene"},
  he:  { votes: 5, "27. 9. 1998": "spd-gruene", "7. 2. 1999": "cdu-fdp", "2. 2. 2003": "cdu", "27. 1. 2008": "cdu geschaeftsfuehrend", "18. 1. 2009": "cdu-fdp", "22. 9. 2013": "cdu-gruene"},
  hh:  { votes: 3, "27. 9. 1998": "spd-gruene", "23. 9. 2001": "cdu-pro-fdp", "29. 2. 2004": "cdu", "24. 2. 2008": "cdu-gruene", "20. 2. 2011": "spd", "15. 2. 2015": "spd-gruene"},
  mv:  { votes: 3, "27. 9. 1998": "spd-linke", "22. 9. 2002": "spd-linke", "17. 9. 2006": "spd-cdu", "4. 9. 2011": "spd-cdu", "4. 9. 2016": "spd-cdu"},
  ni:  { votes: 6, "27. 9. 1998": "spd", "2. 2. 2003": "cdu-fdp", "27. 1. 2008": "cdu-fdp", "20. 1. 2013": "spd-gruene"},
  nrw: { votes: 6, "27. 9. 1998": "spd-gruene", "14. 5. 2000": "spd-gruene", "22. 5. 2005": "cdu-fdp", "9. 5. 2010": "spd-gruene minderheit", "13. 5. 2012": "spd-gruene"},
  rp:  { votes: 4, "27. 9. 1998": "spd-fdp", "25. 3. 2001": "spd-fdp", "26. 3. 2006": "spd", "27. 3. 2011": "spd-gruene", "13. 3. 2016": "spd-fdp-gruene"},
  sh:  { votes: 4, "27. 9. 1998": "spd-gruene", "27. 2. 2000": "spd-gruene", "20. 2. 2005": "spd-gruene geschaeftsfuehrend", "27. 4. 2005": "cdu-spd", "27. 9. 2009": "cdu-fdp", "6. 5. 2012": "spd-gruene-ssw"},
  sl:  { votes: 3, "27. 9. 1998": "spd", "5. 9. 1999": "cdu", "5. 9. 2004": "cdu", "30. 8. 2009": "cdu-fdp-gruene", "19. 1. 2012": "cdu minderheit", "25. 3. 2012": "cdu-spd"},
  sn:  { votes: 4, "27. 9. 1998": "cdu", "19. 9. 1999": "cdu", "19. 9. 2004": "cdu-spd", "30. 8. 2009": "cdu-fdp", "31. 8. 2014": "cdu-spd"},
  st:  { votes: 4, "27. 9. 1998": "spd minderheit", "21. 4. 2002": "cdu-fdp", "26. 3. 2006": "cdu-spd", "20. 3. 2011": "cdu-spd", "13. 3. 2016": "cdu-spd-gruene"},
  th:  { votes: 4, "27. 9. 1998": "cdu-spd", "12. 9. 1999": "cdu", "13. 6. 2004": "cdu", "30. 8. 2009": "cdu-spd", "14. 9. 2014": "linke-spd-gruene"}
};

// All the dates for elections or other actions that resulted in changes of government. Includes brief description to be displayed.
BR.electionExplanations = {
  54: {"17. 9. 2016": "Abgeordnetenhauswahl in Berlin: Erste rot-rot-grüne Koalition unter SPD-Führung."},
  53: {"4. 9. 2016": "Landtagswahlen in Mecklenburg-Vorpommern: Fortsetzung der SPD-CDU-Koalition, AfD zweitstärkste Kraft."},
  52: {"13. 3. 2016": "Landtagswahlen in Baden-Württemberg, Rheinland-Pfalz und Sachsen-Anhalt: AfD triumphiert, Kretschmann regiert mit CDU weiter. Ampel in Rheinland-Pfalz, Kenia-Koaltion in Sachen-Anhalt."},
  51: {"15. 2. 2015": "Bürgerschaftswahl in Hamburg - Olaf Scholz verpasst die absolute Mehrheit knapp, Bildung einer Rot-Grünen Koaltion."},
  50: {"14. 9. 2014": "Landtagswahlen in Brandenburg und Sachsen: Woidke bleibt Ministerpräsident in Brandenburg, erste Rot-Rot-Grüne Koaltion überhaupt in Thüringen."},
  49: {"31. 8. 2014": "Landtagswahl in Sachsen: FDP verliert letzte Regierungsbeteiligung, Neuauflage Schwarz-Rot."},
  48: {"22. 9. 2013": "Landtagswahl in Hessen parallel zur Bundestagswahl: Erste schwarz-grüne Regierung in einem Flächenland."},
  47: {"15. 9. 2013": "Landtagswahl in Bayern: Horst Seehofer holt die absolute Mehrheit zurück."},
  46: {"20. 1. 2013": "Landtagswahl in Niedersachsen: Rot-grün knapp vor der Koalition des Wulff-Nachfolgers McAllister"},
  45: {"13. 5. 2012": "Vorgezogene Landtagswahl in NRW nach gescheitertem Haushalt der rot-grünen Minderheitsregierung"},
  44: {"6. 5. 2012": "Vorgezogene Landtagswahl in Schleswig-Holstein, weil vorheriges Wahlrecht für verfassungswidrig erklärt wurde"},
  43: {"25. 3. 2012": "Vorgezogene Landtagswahl im Saarland nach Auflösung der Jamaika-Koalition."},
  42: {"19. 1. 2012": "Kommissarische CDU-Alleinregierung im Saarland"},
  41: {"18. 9. 2011": "Wahlen zum Abgeordnetenhaus in Berlin"},
  40: {"4. 9. 2011": "Landtagswahl in Mecklenburg-Vorpommern"},
  39: {"22. 5. 2011": "Bürgerschaftswahlen in Bremen, Grüne erstmals vor CDU"},
  38: {"27. 3. 2011": "Landtagswahlen in Baden-Württemberg und Rheinland-Pfalz"},
  37: {"20. 3. 2011": "Landtagswahl in Sachsen-Anhalt"},
  36: {"20. 2. 2011": "Vorgezogene Bürgerschaftswahl nach Koalitionsbruch in Hamburg"},
  35: {"9. 5. 2010": "Landtagswahl in NRW - Bilung einer rot-grünen Minderheitsregierung"},
  34: {"27. 9. 2009": "Vorgezogene Landtagswahl in Schleswig-Holstein nach Bruch der CDU-SPD-Koalition und reguläre Landtagswahlen in Brandenburg"},
  33: {"30. 8. 2009": "Landtagswahlen in Thüringen, Sachsen und im Saarland"},
  32: {"18. 1. 2009": "Vorgezogene Landtagswahl in Hessen - keine erfolgreiche Regierungsbildung mit Linke-Tolerierung"},
  31: {"28. 9. 2008": "Landtagswahl in Bayern, erstmals Verlust der CSU-Mehrheit"},
  30: {"24. 2. 2008": "Bürgerschaftswahl in Hamburg, erstes schwarz-grünes Bündnis"},
  29: {"27. 1. 2008": "Landtagswahlen in Niedersachsen und Hessen"},
  28: {"13. 5. 2007": "Bürgerschaftswahlen in Bremen"},
  27: {"17. 9. 2006": "Landtagswahlen in Berlin und Mecklenburg-Vorpommern"},
  26: {"26. 3. 2006": "Landtagswahlen in Baden-Württemberg, Rheinland-Pfalz und Sachsen-Anhalt"},
  25: {"27. 4. 2005": "Wahl von Peter Harry Carstensen zum Ministerpräsident nach gescheiterter Dänen-Ampel"},
  24: {"22. 5. 2005": "Landtagswahl in NRW führt zu Neuwahlen auf Bundesebene"},
  23: {"20. 2. 2005": "Landtagswahl in Schleswig-Holstein"},
  22: {"19. 9. 2004": "Landtagswahlen in Sachsen und Brandenburg"},
  21: {"5. 9. 2004": "Landtagswahl im Saarland"},
  20: {"13. 6. 2004": "Landtagswahl in Thüringen, Althaus verteidigt absolute Mehrheit"},
  19: {"29. 2. 2004": "Bürgeschaftswahl in Hamburg, Ole von Beust erzielt absolute Mehrheit"},
  18: {"21. 9. 2003": "Landtagswahl in Bayern, CSU erreicht Zweidrittelmehrheit."},
  17: {"25. 5. 2003": "Wahl zur Bremer Bürgerschaft"},
  16: {"2. 2. 2003": "Landtagswahlen in Hessen und Niedersachsen"},
  15: {"22. 9. 2002": "Landtagswahl in Mecklenburg-Vorpommern"},
  14: {"21. 4. 2002": "Landtagswahl in Sachsen-Anhalt"},
  13: {"21. 10. 2001": "Abgeordnetenhauswahl in Berlin nach Bruch der Diepgen-Koalition"},
  12: {"23. 9. 2001": "Bürgerschaftswahl in Hamburg, Koalition mit Ronald Schills PRO"},
  11: {"16. 6. 2001": "Im Berliner Abgeordnetenhaus wird Eberhard Diepgen wird aus dem Amt gewählt, rot-grüne Minderheitsregierung in Berlin"},
  10: {"25. 3. 2001": "Landtagswahlen in Rheinland-Pfalz und Baden-Württemberg"},
  9: {"14. 5. 2000": "Landtagswahl in NRW weiterhin in der Nachwirkung der CDU-Spendenaffäre"},
  8: {"27. 2. 2000": "Landtagswahl in Schleswig-Holstein, die erste nach der CDU-Spendenaffäre"},
  7: {"10. 10. 1999": "Wahl zum Berliner Abgeordnetenhaus"},
  6: {"19. 9. 1999": "Landtagswahl in Sachsen"},
  5: {"12. 9. 1999": "Landtagswahl in Thüringen - absolute Mehrheit für Bernhard Vogel"},
  4: {"5. 9. 1999": "Landtagswahlen im Saarland und in Brandenburg - jeweils Verlust der absoluten Mehrheit für die SPD"},
  3: {"6. 6. 1999": "Wahl zur Bremer Bürgerschaft"},
  2: {"7. 2. 1999": "Landtagswahl in Hessen nach der Unionskampagne gegen doppelte Staatsbürgerschaft"},
  1: {"27. 9. 1998": "Landtagswahl in Mecklenburg-Vorpommern zeitgleich zur Bundestagswahl"}
};

// Function returns the date from the from by accessing the election data object in a for 'loop'/with Object.keys
// shorthand function to increase overall code legibility
BR.dateFromForm = function dateFromForm(formValue) {
  if (formValue === "" || formValue === undefined) {
    formValue = Object.keys(BR.electionExplanations).length;
  }
  return String(Object.keys(BR.electionExplanations[formValue]));
};

// Helper function that returns the last election that was held in a state up to a given date
BR.lastElection = function lastElection(state, upToThisDate) {
  var auxdate,
    prevDate;
  if (upToThisDate === "") {
    upToThisDate = Object.keys(BR.electionExplanations).length;
  }
  if (!(BR.laender[state][BR.dateFromForm(upToThisDate)])) {
    prevDate = upToThisDate;
    do {
      prevDate--;
      auxdate = String(Object.keys(BR.electionExplanations[prevDate]));
    } while (!(BR.laender[state][auxdate]) && (prevDate > 0));
    return auxdate;
  } else {
    return BR.dateFromForm(upToThisDate);
  }
};

// General reset function to clear tables and objects
BR.resetlists = function resetLists() {
  BR.coalitionvotes = {};
  BR.partystates = {};
  document.getElementById("coalitiontable").querySelector("tbody").innerHTML = "";
  document.getElementById("partytable").querySelector("tbody").innerHTML = "";
  document.getElementById("governortable").querySelector("tbody").innerHTML = "";
};

// Helper function to pre-load the assets for the background of the tables and the canvas areas
function preload() {
  BR.objCount = 0;
  BR.loadCount = 0;
  BR.imageObj = [];

  BR.coalitions.forEach(function (coalition) {
    BR.imageObj[coalition] = new Image();
    BR.imageObj[coalition].onload = BR.loadCount++;
    BR.imageObj[coalition].src = "img/" + coalition + ".png";
  });
}

// Converts the internally used party names from a given party or coalition string to a displayable option.
// Simple rule: three-letter parties in all caps, others with just their first letter capitalised
BR.partyname = function partyName (rawName) {
  var i,
    partynames;
  rawName = rawName.split(" ")[0];
// TODO: Restore
//  if (name === "linke"&& $("#dateselector").length > 0) {
//    if (BR.dateFromForm($("#dateselector").val()).split(" ")[2] < 2005) {
//      name = "pds";
//    }
//  }
  if (rawName.indexOf("-") === -1) {
    if (rawName.length === 3) {
      return rawName.toUpperCase();
    } else {
      return rawName.substr(0, 1).toUpperCase() + rawName.substr(1);
    }
  } else {
    partynames = rawName.split("-");
    for (i = 0; i < partynames.length; i++) {
      partynames[i] = BR.partyname(partynames[i]);
    }
    return partynames.join("-");
  }
};

BR.getStateNamesFromDOM = function getStateNamesFromDOM(nodeList) {
  return Array.prototype.map.call(nodeList, function (state) {
    return state.title;
  });
};

// Updates the information tables and sets event handlers for the tooltipps and the div highlighting
// TODO: We should not do this individual appending etc.
BR.tableUpdate = function tableUpdate(selector, selectorvotes, item) {
  var
    selectorTable = document.getElementById(selector + "table").querySelector("tbody"),
    voteContainer = document.getElementById("votecontainer"),
  // TODO: Refactor, this is quite horrible - get it from the data, not the DOM
    matchingStates = selector !== "party" ? voteContainer.querySelectorAll("." + item) :
      Array.prototype.filter.call(voteContainer.children, function (state) {
        return state.className.indexOf(item) !== -1;
      }),
    stateNames = BR.getStateNamesFromDOM(matchingStates),
    selectorDescription = selector !== "party" ? "-Regierung: " : "-Regierungsbeteiligungen: ",
    tooltipSpan = "Länder mit "  + BR.partyname(item) + selectorDescription + stateNames.join(", "),
    newTr = document.createElement("tr"),
    trMarkup = '<td><span data-tooltip aria-haspopup="true" class="has-tip" title="'  + tooltipSpan +
      '">'  + BR.partyname(item) + '</span></td><td>'+ selectorvotes + '</td>';

  newTr.className = item;
  newTr.innerHTML = trMarkup;
  selectorTable.appendChild(newTr);
};

// Helper function to loop through the amount of possible votes for parties/coalitions and call the tableUpdate function
BR.textinfo = function textinfo() {
  var
    allStates,
    governors = {},
    i,
    item,
    trMarkup = "",
    helper;
  for (i = 37; i > 0; i--) {
    for (item in BR.coalitionvotes) {
      if (BR.coalitionvotes.hasOwnProperty(item) && BR.coalitionvotes[item] === i) {
        BR.tableUpdate("coalition", BR.coalitionvotes[item], item);
      }
    }
    if (i < 17) {
      for (helper in BR.partystates) {
        if (BR.partystates.hasOwnProperty(helper) && BR.partystates[helper] === i) {
          BR.tableUpdate("party", BR.partystates[helper], helper);
        }
      }
    }
  }


  allStates = document.getElementById("votecontainer").children;
  Array.prototype.forEach.call(allStates, function (state) {
    var
      theGovernor = state.classList[0].split("-")[0];
    if (governors.hasOwnProperty(theGovernor)) {
      governors[theGovernor] += 1;
    } else {
      governors[theGovernor] = 1;
    }
  });

  for (i in governors) {
    if (governors.hasOwnProperty(i)) {
      trMarkup += '<tr><td><span>'  + BR.partyname(i) + '</span></td><td>'+ governors[i] + '</td></tr>';
    }
  }

  document.getElementById("governortable").querySelector("tbody").innerHTML = trMarkup;

  $("#coalitiontable, #partytable").foundation("tooltip", "reflow");
};

// Calculates and updates the number of governments a party is in, filtering out minority and commissionary governments
BR.partycount = function partycount(parties) {
  var i,
    partysplit = parties.split(" ")[0].split("-");
  for (i = 0; i < partysplit.length; i++) {
    BR.partystates[partysplit[i]] > 0 ? BR.partystates[partysplit[i]]++ : BR.partystates[partysplit[i]] = 1;
  }
};

// Calculates and updates the number of governments a coalition holds, filtering out minority and commissionary governments
BR.coalitioncount = function coalitioncount(coalition, votenumber) {
  coalition = coalition.split(" ")[0];
  BR.coalitionvotes[coalition] > 0 ? BR.coalitionvotes[coalition] += votenumber : BR.coalitionvotes[coalition] = votenumber;
};

// Fills the non-canvas version of the map with the correct coalition backgrounds
BR.landfill = function landfill(arraynumber) {
  var
    land,
    lastElectionInfo,
    selectedDate = BR.dateFromForm(document.getElementById("dateselector").getAttribute("data-slider"));

  document.getElementById("explanation").innerHTML = selectedDate + ": " + BR.electionExplanations[arraynumber][selectedDate];

  for (land in BR.laender) {
    if (BR.laender.hasOwnProperty(land)) {
      lastElectionInfo = BR.lastElection(land, arraynumber);
      document.getElementById(land).className = BR.laender[land][lastElectionInfo];
      BR.coalitioncount(BR.laender[land][lastElectionInfo], BR.laender[land]['votes']);
      BR.partycount(BR.laender[land][lastElectionInfo]);
    }
  }
};

$(function () {
  var lastElectionIndex = Object.keys(BR.electionExplanations).length,
    $dateSelector = $("#dateselector"),
    $explanation = $("#explanation"),
    electionDay;
  BR.resetlists();
  BR.landfill(lastElectionIndex);
  BR.textinfo();
  // TODO: Since we use the foundation slider, do we need support for native range?
  if (supports_range()) {
    $dateSelector.attr({
      "data-options": "start: 1; end:" +  lastElectionIndex + ";",
      "data-slider": lastElectionIndex
    });
  } else {
    $explanation.before('Datum: <select id="dateselector" ></select>');
    for (electionDay in BR.electionExplanations) {
      if (BR.electionExplanations.hasOwnProperty(electionDay)) {
        $dateSelector.append("<option value=" + electionDay + " selected>" + BR.dateFromForm(electionDay) + "</option>");
      }
    }
  }

  $explanation.html('<strong>' + BR.dateFromForm(lastElectionIndex) + '</strong>: ' + BR.electionExplanations[lastElectionIndex][BR.dateFromForm(lastElectionIndex)]);
  $("#zeit-api-link").on("click", function() {
    BR.getZeitArticles(BR.dateFromForm($("#dateselector").attr("data-slider")));
  });
  if (supports_canvas()) {
    preload();
    BR.loadtester = setInterval(function () {
      if (BR.loadCount === BR.objCount) {
        BR.drawBundesrat();
        BR.drawGermany($dateSelector.val());
        BR.drawCartogram($dateSelector.val());
        $('#geolink').addClass('activetab');
        clearInterval(BR.loadtester);
      }
    }, 100);
  } else {
    $("#maptab, #germanymap, #cartogram").remove();
    $("#votes").show();
  }

  $dateSelector.on('change.fndtn.slider', function () {
    var
      number,
      dateselectorval = $("#dateselector").attr("data-slider"),
      dateFromFormVal = BR.dateFromForm(dateselectorval);

    //resetlists and draw also need to be within loop to work for the 1st value
    for (number in BR.electionExplanations[dateselectorval]) {
      $("#explanation").html('<strong>' + dateFromFormVal + '</strong>: ' + BR.electionExplanations[dateselectorval][dateFromFormVal]);
      $("#zeit-api-link").show().off().on("click", function() {
        BR.getZeitArticles(dateFromFormVal);
      });
      $("#zeit-api-news").find("div").empty();
      BR.resetlists();
      BR.landfill(dateselectorval);
      BR.textinfo();
      BR.drawBundesrat();
    }

    BR.drawGermany(dateselectorval);
    BR.drawCartogram(dateselectorval);

  });
});