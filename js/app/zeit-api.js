// Querying the ZEIT API for articles. Since the API does not have a query directly by date, the results have to be filtered.
// Let's start by building the query - we want to include the L�nder that had elections and the term 'Wahlen'

BR.getZeitArticles = function getZeitArticles(dateFromFormVal) {
	var electionstates = [],
    milliSecondsInWeek = 604800000,
	  $zeitapinews = $("#zeit-api-news");
	$zeitapinews.show().css({"overflow-y" : "hidden", "background" : "url(img/spinningwheel.gif) center center no-repeat", "min-height" : "100px"}).find("div").empty();

	for (var key in BR.laender) {
    var obj,
      prop;
    if (BR.laender.hasOwnProperty(key)) {
      obj = BR.laender[key];
      for (prop in obj) {
        if (prop && (prop === dateFromFormVal)) {
          electionstates.push($("#votecontainer").find("[id=" + key + "]").map(function () {
            return $(this).attr('title');
          }).get().join());
        }
      }
		}
	}

  // TODO: Write dates in js-compatible format to begin with
  var electiondate = new Date(parseInt(dateFromFormVal.split(" ")[1]) + "/" +  parseInt(dateFromFormVal.split(" ")[0]) + "/" + parseInt(dateFromFormVal.split(" ")[2])),
    startdate = new Date(electiondate - milliSecondsInWeek),
    enddate = new Date(electiondate - 0 + milliSecondsInWeek),
    startdateString = startdate.getFullYear() + "-" + (startdate.getMonth()+1) + "-" + startdate.getDate() + "T23:00:00.000Z",
    enddateString = enddate.getFullYear() + "-" + (enddate.getMonth()+1) + "-" + enddate.getDate() + "T21:59:59.000Z";

  $.getJSON('http://api.zeit.de/content?q=Wahl+' + electionstates.toString().split(",").join("+") + '+AND+release_date:[' +
    startdateString + ' TO ' + enddateString + ']&limit=10&fields=release_date,href,title,subtitle' +
    '&api_key=bb62f095254930eb4459cbd703c09bd48340c0614c020257d083', function(data) {
		$.each(data, function(key, val) {
		if (key === "matches") {
      $("#zeit-api-link").hide();
      $.each(val, function(matcheskey, matchesval) {
			  var articleText = "<h5><a href=\"" + matchesval.href + "\">" + matchesval.title + "</a></h5>";
        if (matchesval.subtitle && matchesval.subtitle.length > 0) {
          articleText += "<p class='zeit-snippet'>" + matchesval.subtitle + "</p>";
        } else {
          articleText += "<p class='zeit-snippet'>[kein Untertitel vorhanden]</p>"
        }
        $zeitapinews.find("div").append(articleText);
			});
			}
		});
    $zeitapinews.css("background","none");
    $zeitapinews.find("div").removeClass("hide");

    if ($zeitapinews.find("a:visible").length === 0) {
      $zeitapinews.find("div").append("Leider scheint es keine passenden Artikel im Zeit-Archiv zu geben.");
    }
	});
};