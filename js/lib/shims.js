// Helper function based on prototype.js implementation to return key to an object.
if (!Object.keys) { 
	Object.keys = function( obj ) {
	  var array = new Array();
	  for ( var prop in obj ) {
		if ( obj.hasOwnProperty( prop ) ) {
		  array.push( prop );
		}
	  }
	  return array;
	}
  };