/* Based on Dive into HTML5 */

function supports_canvas() {
  return !!document.createElement('canvas').getContext;
}

/* Based on Modenizr */

function supports_range() {

var rangeElem = document.createElement('input'), bool;

rangeElem.setAttribute("type", "range");
bool = rangeElem.type !== 'text';

if ( bool ) {
	rangeElem.value         = ":-)";
	rangeElem.style.cssText = 'position:absolute;visibility:hidden;';

	if (rangeElem.style.WebkitAppearance !== undefined ) {
		document.documentElement.appendChild(rangeElem);
		defaultView = document.defaultView;
		bool =  defaultView.getComputedStyle && defaultView.getComputedStyle(rangeElem, null).WebkitAppearance !== 'textfield' && (rangeElem.offsetHeight !== 0);
		document.documentElement.removeChild(rangeElem);
	} else {
		bool = rangeElem.value != ":-)";
	}
}

 return bool;
}